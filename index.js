const express = require('express')
const app = express()
const {PORT = 8080} = process.env
const path = require('path')

app.use('/assets', express.static(path.join(__dirname, 'public', 'static')))

app.get('/', (req, res) => {
    return res.sendFile(path.join(__dirname, 'public', 'index.html'))
})

app.get('/restaurants', (req, res) => {
    return res.sendFile(path.join(__dirname, 'public', 'restaurants.html'))
})

app.get('/data', (req, res) => {
    return res.sendFile(path.join(__dirname, 'data.json'))
})

app.listen(PORT, () => console.log('Server started on port ${PORT}...'))